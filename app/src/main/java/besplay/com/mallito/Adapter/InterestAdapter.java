//package besplay.com.mallito.Adapter;
//
//import android.content.Context;
//import android.graphics.Typeface;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.CheckBox;
//import android.widget.CompoundButton;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.bumptech.glide.Glide;
//import com.bumptech.glide.load.engine.DiskCacheStrategy;
//
//import java.util.List;
//
//import besplay.com.mallito.Classes.Interest;
//import besplay.com.mallito.R;
//
//
///**
// * Created by Besplay on 3/13/2017.
// */
//
//public class InterestAdapter extends RecyclerView.Adapter<InterestAdapter.MyInterestHolder> {
//
//
//    private List<Interest> interestList;
//    private Context context;
//
//    public static class MyInterestHolder extends RecyclerView.ViewHolder {
//
//        private ImageView interestImage;
//        private TextView interestTitle;
//        private CheckBox checkBox;
//
//        public MyInterestHolder(View itemView) {
//            super(itemView);
//
//            interestImage = (ImageView) itemView.findViewById(R.id.interestImage);
//            interestTitle = (TextView) itemView.findViewById(R.id.interestTitle);
//            checkBox = (CheckBox) itemView.findViewById(R.id.interestCheckbox);
//        }
//    }
//
//
//    public InterestAdapter(List<Interest> interestList, Context context) {
//        this.interestList = interestList;
//        this.context = context;
//
//    }
//
//    @Override
//    public MyInterestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.interest_item, parent, false);
//        return new MyInterestHolder(itemView);
//    }
//
//
//    @Override
//    public void onBindViewHolder(final MyInterestHolder holder, final int position) {
//        Interest interest = interestList.get(position);
//
//
//        Glide
//                .with(context)
//                .load(interest.getImageResourceId())
//                .thumbnail(0.9f)
//                .placeholder(R.drawable.ic_mallitoplaceholder)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .crossFade()
//                .into(holder.interestImage);
//
//        Typeface openSansBold = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Bold.ttf");
//
//        holder.interestTitle.setTypeface(openSansBold);
//        holder.interestTitle.setText(interest.getTitle());
//
//        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                int getPosition = (Integer) buttonView.getTag();
//                interestList.get(getPosition).setSelected(buttonView.isChecked());
//            }
//        });
//
//        holder.checkBox.setTag(position);
//        holder.checkBox.setChecked(interestList.get(position).isSelected());
//
//        holder.interestImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                holder.checkBox.setChecked(!holder.checkBox.isChecked());
//            }
//
//        });
//
//
//    }
//
//    @Override
//    public int getItemCount() {
//        return interestList.size();
//    }
//
//
//}
