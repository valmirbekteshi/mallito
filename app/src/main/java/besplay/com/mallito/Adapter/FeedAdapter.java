package besplay.com.mallito.Adapter;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import besplay.com.mallito.Classes.Feed;
import besplay.com.mallito.Helpers.MallitoHelper;
import besplay.com.mallito.R;


/**
 * Created by Besplay on 3/13/2017.
 */

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.MyProductHolder> {


    private List<Feed> productList;
    private Context context;
    MallitoHelper mallitoHelper;

    public static class MyProductHolder extends RecyclerView.ViewHolder {

        private ImageView productImage;
        private TextView documentType;
        private TextView url;
        private TextView title;
        private TextView newPrice;
        private TextView price;
        private TextView listSale;


        public MyProductHolder(View itemView) {
            super(itemView);

            productImage = (ImageView) itemView.findViewById(R.id.productImage);
            documentType = (TextView) itemView.findViewById(R.id.documentType);
            url = (TextView) itemView.findViewById(R.id.url);
            title = (TextView) itemView.findViewById(R.id.title);
            newPrice = (TextView) itemView.findViewById(R.id.newPrice);
            price = (TextView) itemView.findViewById(R.id.price);
            listSale = (TextView) itemView.findViewById(R.id.listSale);

        }
    }


    public FeedAdapter(List<Feed> productList, Context context) {
        this.productList = productList;
        this.context = context;

    }


    @Override
    public MyProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new MyProductHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyProductHolder holder, int position) {
        final Feed product = productList.get(position);

        Typeface openSansRegular = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface openSansBold = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Bold.ttf");
        Typeface openSansSemiBold = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Semibold.ttf");

        String thmb = product.getImageUrl().replaceAll("crp", "thumb");


        if (product.getImageUrl().isEmpty()) {
            holder.productImage.setImageResource(R.drawable.ic_mallitoplaceholder);
        } else {

            Glide
                    .with(context)
                    .load(product.getImageUrl())
                        .thumbnail(0.9f)
                        .placeholder(R.drawable.ic_mallitoplaceholder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .crossFade()
                    .into(holder.productImage);
        }


        holder.title.setTypeface(openSansRegular);
        holder.documentType.setTypeface(openSansRegular);
        holder.url.setTypeface(openSansSemiBold);
        holder.newPrice.setTypeface(openSansBold);
        holder.listSale.setTypeface(openSansRegular);


        mallitoHelper = new MallitoHelper();

        int percentage = mallitoHelper.percentage(product);


        if (percentage > 0) {
            holder.listSale.setText(percentage + " % off");
        } else {
            holder.listSale.setVisibility(View.GONE);
        }


        holder.url.setText(product.getPublisherName());



//        holder.price.setText(product.getPrice()+" €");
//        holder.newPrice.setText(product.getNewPrice()+" €");


        holder.price.setVisibility(View.GONE);
        holder.newPrice.setVisibility(View.GONE);


        // TODO: //nese NEW PRICE NUK VJEN HIQ ATHERE SRIKE THROUGH QITJA JEKJA PRICIT KREJT

        if (product.getPrice()!= null && !product.getPrice().equals("0") && !product.getPrice().isEmpty()) {


            holder.price.setText(product.getPrice()+" €");
            holder.price.setTextColor((Color.parseColor("#999999")));
            holder.price.setVisibility(View.VISIBLE);
            holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

       if (product.getNewPrice() != null &&  !product.getNewPrice().equals("0") && !product.getNewPrice().isEmpty()) {

            holder.newPrice.setText(product.getNewPrice()+" €");
            holder.newPrice.setVisibility(View.VISIBLE);

        }
        else{

            holder.price.setPaintFlags(holder.price.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
            holder.price.setTextColor(Color.BLACK);
       }


        switch (product.getDocumentType()) {

            case "gallery":
                holder.price.setVisibility(View.GONE);
                holder.newPrice.setVisibility(View.GONE);
                holder.documentType.setBackgroundResource(R.color.galleryColor);
                break;
            case "product":
                holder.documentType.setBackgroundResource(R.color.productColor);
                break;
            case "jobpost":
                holder.price.setVisibility(View.GONE);
                holder.newPrice.setVisibility(View.GONE);
                holder.documentType.setBackgroundResource(R.color.jobPostColor);
                break;


            case "blog":
                holder.price.setVisibility(View.GONE);
                holder.newPrice.setVisibility(View.GONE);
                holder.documentType.setBackgroundResource(R.color.blogColor);
                break;

            case "general":
                holder.documentType.setBackgroundResource(R.color.generalColor);
                break;
        }


        holder.documentType.setText(product.getDocumentType());

        holder.title.setText(product.getTitle());


        //animation

//        int lastPosition = -1;
//
//        Animation animation = AnimationUtils.loadAnimation(context,
//                (position > lastPosition) ? R.anim.up_from_bottom
//                        : R.anim.down_from_top);
//        holder.itemView.startAnimation(animation);
//        lastPosition = position;

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }


    @Override
    public void onViewDetachedFromWindow(MyProductHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }
}
