package besplay.com.mallito.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import besplay.com.mallito.ApiInterface.ApiInterface;
import besplay.com.mallito.Classes.Cat;
import besplay.com.mallito.R;
import besplay.com.mallito.rest.ApiClient;

/**
 * Created by Visari on 4/26/2017.
 */

public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.MyProductHolder> {

    private List<Cat> menuList;
    private Context context;
    private RecyclerView drawerList;


    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


    public static class MyProductHolder extends RecyclerView.ViewHolder {

        private TextView drawerListItem;
        private TextView arrow;
        ImageView arrow1;
        private LinearLayout linearItem;

        public MyProductHolder(View itemView) {
            super(itemView);

            drawerListItem = (TextView) itemView.findViewById(R.id.drawerListItem);
            //arrow = (TextView) itemView.findViewById(R.id.arrow);
            linearItem = (LinearLayout) itemView.findViewById(R.id.linearItem);
            arrow1 = (ImageView)itemView.findViewById(R.id.arrow);
        }
    }

    public DrawerAdapter(List<Cat> menuList, Context context, RecyclerView recyclerView) {
        this.menuList = menuList;
        this.context = context;
        this.drawerList = recyclerView;
    }

    @Override
    public DrawerAdapter.MyProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.drawer_list_item, parent, false);
        return new DrawerAdapter.MyProductHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DrawerAdapter.MyProductHolder holder, final int position) {
        final Cat cat = menuList.get(position);

        Typeface openSansRegular = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface openSansSemiBold = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Semibold.ttf");

//        if (cat.getSubcategories() == null || cat.getSubcategories().size() == 0) {
//            holder.arrow.setVisibility(View.GONE);
//        } else {
//            holder.arrow.setVisibility(View.VISIBLE);
//        }

        if (cat.getSubcategories() == null || cat.getSubcategories().size() == 0) {
            holder.arrow1.setVisibility(View.GONE);
        } else {
            holder.arrow1.setVisibility(View.VISIBLE);
        }

        holder.drawerListItem.setTypeface(openSansSemiBold);
        holder.drawerListItem.setText(cat.getName());

//        holder.drawerListItem.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(context, cat.getId().toString() + " - " + cat.getName(), Toast.LENGTH_SHORT).show();
//
//
//                Intent i = new Intent(context, MainActivity.class);
//                i.putExtra("catId", cat.getId());
//
//
//                if (cat.getParentId() == 0 && cat.getSubcategories() != null) {
//                    nav_second.setText(" -> " + cat.getName() + " -> ");
//                } else if (cat.getParentId() > 0 && cat.getSubcategories().size() == 0) {
//                } else {
//                    nav_third.setText(cat.getName());
//                }
//
//                if (cat.getSubcategories() == null) {
//                } else {
//                    menuList.clear();
//
//                    for (Subcategory sub : cat.getSubcategories()) {
//                        Cat category = new Cat();
//                        category.setId(sub.getId());
//                        category.setName(sub.getName());
//                        category.setDt(sub.getDt());
//                        category.setParentId(sub.getParentId());
//
//                        List<Subcategory> subcategory_list = new ArrayList<Subcategory>();
//
//                        if (sub.getSubcategories() == null) {
//                        } else {
//                            for (Subcategory_ subcategory_ : sub.getSubcategories()) {
//                                Subcategory subcategory = new Subcategory();
//                                subcategory.setId(subcategory_.getId());
//                                subcategory.setName(subcategory_.getName());
//                                subcategory.setParentId(subcategory_.getParentId());
//                                subcategory.setDt(subcategory_.getDt());
//                                subcategory_list.add(subcategory);
//                            }
//                        }
//
//                        category.setSubcategories(subcategory_list);
//                        menuList.add(category);
//                    }
//
//                    if (menuList.size() > 0) {
//                        drawerList.setAdapter(new DrawerAdapter(menuList, context, drawerList));
//                    }
//                }
//            }
//        });

        Animation animation = AnimationUtils.loadAnimation(context, R.anim.push_left_in);
        holder.linearItem.startAnimation(animation);
    }

    @Override
    public int getItemCount() {
        return menuList.size();
    }
}