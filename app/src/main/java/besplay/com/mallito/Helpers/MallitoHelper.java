package besplay.com.mallito.Helpers;

import java.util.regex.Pattern;

import besplay.com.mallito.Classes.Feed;

/**
 * Created by Valmir on 4/18/2017.
 */

public class MallitoHelper {
    private static Pattern patternPrice = Pattern.compile("(\\d+[[\\.€,\\s]\\d+]*)");

    public int percentage(Feed feed) {

        if (feed.getPrice() == null || feed.getNewPrice() == null ||
                feed.getPrice().equals("") || feed.getNewPrice().equals("")
                || feed.getPrice().startsWith("0")|| feed.getNewPrice().startsWith("0")
                || feed.getPrice().equals("0.00") || feed.getNewPrice().equals("0.00")) {
            return 0;
        } else {

//            double price = ext(feed.getPrice());
//            double newPrice = ext(feed.getNewPrice());
//            double sale = ((Double.valueOf(feed.getPrice()) - Double.valueOf(feed.getNewPrice()))) / Double.valueOf(feed.getNewPrice())* 100;

            double  discountPrice = Double.valueOf(feed.getPrice()) - Double.valueOf(feed.getNewPrice());
            double discountRate = discountPrice / Double.valueOf(feed.getPrice());

//            if (Double.isNaN(discountRate))
//            {
//                return 0;
//            }

            return (int)Math.round(discountRate * 100);

//            return sale;
        }
    }


    public boolean validatePrices(String feed){
        boolean validate = false;


        if(feed != null || !feed.isEmpty() || !feed.equals("0")){
            validate = true;
        }

        return validate;
    }


    public static double calcDiscountPercentage(double price, double new_price)
    {

        double  discountPrice = price - new_price;
        double discountRate = discountPrice / price;

        if (Double.isNaN(discountRate))
        {
            return 0;
        }

        return Math.round(discountRate * 100);
    }




    static double calcuateDiscount(double markedprice,double s)
    {

        double amount= (s*markedprice)/100;

        return amount;

    }



    public  double ext(String nr){

        String replace = nr.replaceAll("[<>\\[\\],$€£Euro.-]", "");

        try {
            return Double.parseDouble(replace);
        }

        catch (NumberFormatException e){

            e.getMessage();
        }

     return 0;
    }






}
