//package besplay.com.mallito.ui;
//
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.v7.app.AppCompatActivity;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.auth.FirebaseAuth;
//
//import besplay.com.mallito.R;
//
//public class ForgotPassword extends AppCompatActivity {
//    EditText emailBox;
//    Button resetPassword;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_forgot_password);
//
//
//        final TextView emailBox = (TextView) findViewById(R.id.emailBox);
//        Button resetPassword = (Button) findViewById(R.id.resetButton);
//
//
//
//        resetPassword.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FirebaseAuth auth = FirebaseAuth.getInstance();
//                final String emailAddress = emailBox.getText().toString();
//
//                auth.sendPasswordResetEmail(emailAddress)
//                        .addOnCompleteListener(new OnCompleteListener<Void>() {
//                            @Override
//                            public void onComplete(@NonNull Task<Void> task) {
//                                if (task.isSuccessful()) {
//                                    Toast.makeText(ForgotPassword.this, "Email to reset your password was sent to "+emailAddress, Toast.LENGTH_SHORT).show();
//                                }
//                            }
//                        });
//            }
//        });
//
//
//    }
//}
