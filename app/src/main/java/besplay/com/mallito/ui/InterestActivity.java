//package besplay.com.mallito.ui;
//
//import android.content.Intent;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.CheckBox;
//import android.widget.TextView;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import besplay.com.mallito.Adapter.InterestAdapter;
//import besplay.com.mallito.Classes.Interest;
//import besplay.com.mallito.MainActivity;
//import besplay.com.mallito.R;
//
//
//public class InterestActivity extends AppCompatActivity {
//
//
//    RecyclerView recyclerView;
//    private List<Interest> interestList;
//    InterestAdapter adapter;
//    CheckBox checkBox;
//    public static int countInterest;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_interest);
//
//
//        LayoutInflater inflater = InterestActivity.this.getLayoutInflater();
//        final View inflateView = inflater.inflate(R.layout.interest_item, null);
//
//        adapter = new InterestAdapter(getInterest(), getApplicationContext());
//        checkBox = (CheckBox) inflateView.findViewById(R.id.interestCheckbox);
//        recyclerView = (RecyclerView) findViewById(R.id.recycleViewInterest);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        recyclerView.setAdapter(adapter);
//
//
//        TextView mallitoText = (TextView) findViewById(R.id.mallitoLogo);
//        TextView interestSubtitle = (TextView) findViewById(R.id.interestSubtitle);
//        TextView interestBottomTitle = (TextView) findViewById(R.id.interestBottomTitle);
//        TextView interestContinueButton = (TextView) findViewById(R.id.interestContinueButton);
//
//        Typeface poppinsSemiBold = Typeface.createFromAsset(getAssets(), "fonts/Poppins-SemiBold.ttf");
//        Typeface openSansRegular = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
//
//
//        mallitoText.setTypeface(poppinsSemiBold);
//        interestSubtitle.setTypeface(openSansRegular);
//        interestBottomTitle.setTypeface(openSansRegular);
//        interestContinueButton.setTypeface(openSansRegular);
//
//
//        interestContinueButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                if (countInterest <= 6) {
////                    Toast.makeText(InterestActivity.this, "Duhet te zgjedhni se paku 6 interesa", Toast.LENGTH_SHORT).show();
////                } else {
//                    Intent i = new Intent(InterestActivity.this, MainActivity.class);
//                    startActivity(i);
//
////                }
//
//            }
//        });
//
//    }
//
//
//
//    public List<Interest> getInterest() {
//
//        List<Interest> list = new ArrayList<>();
//
//        list.add(new Interest("Food", R.drawable.picone));
//        list.add(new Interest("Fashion", R.drawable.pic2));
//        list.add(new Interest("Transportation", R.drawable.pic3));
//        list.add(new Interest("Technology", R.drawable.pic4));
//        list.add(new Interest("Well-Being", R.drawable.pic5));
//        list.add(new Interest("HOME", R.drawable.pic6));
//        list.add(new Interest("TOOLS", R.drawable.pic7));
//        list.add(new Interest("Fashion", R.drawable.pic8));
//        list.add(new Interest("Fashion", R.drawable.pic9));
//        list.add(new Interest("Fashion", R.drawable.pic10));
//        list.add(new Interest("Fashion", R.drawable.pic11));
//        list.add(new Interest("Fashion", R.drawable.pic12));
//        list.add(new Interest("Fashion", R.drawable.pic13));
//        list.add(new Interest("Fashion", R.drawable.pic14));
//        list.add(new Interest("Hello", R.drawable.pic15));
//
//        return list;
//    }
//}
