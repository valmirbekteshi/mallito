//package besplay.com.mallito.ui;
//
//import android.content.Intent;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Toast;
//
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.auth.AuthResult;
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.auth.FirebaseUser;
//
//import besplay.com.mallito.Helpers.EmailValidator;
//import besplay.com.mallito.MainActivity;
//import besplay.com.mallito.R;
//
//public class SignupActivity extends AppCompatActivity {
//
//    EditText emailEditText, passwordEditText, retypePasswordEditText;
//    Button signUpButton;
//    private FirebaseAuth mAuth;
//    private EmailValidator emailValidator;
//    private String TAG = SignupActivity.class.getSimpleName();
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_signup);
//
//        Typeface openSansRegular = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
//
//        emailEditText = (EditText) findViewById(R.id.emailEditText);
//        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
//        retypePasswordEditText = (EditText) findViewById(R.id.retypePasswordEditText);
//        signUpButton = (Button) findViewById(R.id.signUpButton);
//
//        mAuth = FirebaseAuth.getInstance();
//
//        emailValidator = new EmailValidator();
//
//        signUpButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                boolean valid = emailValidator.validate(emailEditText.getText().toString());
//
//
//                    //createUserWithEmailAndPassword(emailEditText.getText().toString(), passwordEditText.getText().toString());
//
//                mAuth.createUserWithEmailAndPassword(emailEditText.getText().toString(), passwordEditText.getText().toString())
//                        .addOnCompleteListener(SignupActivity.this, new OnCompleteListener<AuthResult>() {
//                            @Override
//                            public void onComplete(@NonNull Task<AuthResult> task) {
//                                if (task.isSuccessful()) {
//
//                                    sendEmailVerification();
//                                    // Sign in success, update UI with the signed-in user's information
//                                    Log.d("", "createUserWithEmail:success");
//                                    Toast.makeText(SignupActivity.this, "Signup SUCESS, Email Verification Send.",
//                                            Toast.LENGTH_SHORT).show();
//                                    FirebaseUser user = mAuth.getCurrentUser();
//
//                                } else {
//                                    // If sign in fails, display a message to the user.
//                                    Log.w("", "createUserWithEmail:failure", task.getException());
//                                    Toast.makeText(SignupActivity.this, "Authentication failed.",
//                                            Toast.LENGTH_SHORT).show();
//
//                                }
//                            }
//                        });
//
//
//            }
//        });
//
//
//    }
//
//    public void createUserWithEmailAndPassword(String email, String password) {
//
//        mAuth.createUserWithEmailAndPassword(email, password)
//                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        if (task.isSuccessful()) {
//                            // Sign in success, update UI with the signed-in user's information
//                            Log.d(TAG, "createUserWithEmail:success");
//                            FirebaseUser user = mAuth.getCurrentUser();
//
//                            startActivity(new Intent(SignupActivity.this, MainActivity.class));
//
//                        } else {
//                            // If sign in fails, display a message to the user.
//                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
//                            Toast.makeText(SignupActivity.this, "Authentication failed.",
//                                    Toast.LENGTH_SHORT).show();
//
//                        }
//
//                    }
//                });
//    }
//
//    private void sendEmailVerification() {
//
//        // Send verification email
//        // [START send_email_verification]
//        final FirebaseUser user = mAuth.getCurrentUser();
//        user.sendEmailVerification()
//                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        // [START_EXCLUDE]
//                        // Re-enable button
//
//                        if (task.isSuccessful()) {
//                            Toast.makeText(SignupActivity.this,
//                                    "Verification email sent to " + user.getEmail(),
//                                    Toast.LENGTH_SHORT).show();
//                        } else {
//                            Log.e("Tag", "sendEmailVerification", task.getException());
//                            Toast.makeText(SignupActivity.this,
//                                    "Failed to send verification email.",
//                                    Toast.LENGTH_SHORT).show();
//                        }
//                        // [END_EXCLUDE]
//                    }
//                });
//        // [END send_email_verification]
//    }
//}
