//package besplay.com.mallito.ui;
//
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Typeface;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.auth.AuthResult;
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.auth.FirebaseUser;
//
//import besplay.com.mallito.Helpers.EmailValidator;
//import besplay.com.mallito.MainActivity;
//import besplay.com.mallito.R;
//
//
//public class LoginActivity extends AppCompatActivity {
//
//
//    private EditText emailEditText;
//    private EditText passwordEditText;
//    private Button loginButton;
//    private TextView forgotPasswordTextView;
//    private EmailValidator emailValidator;
//    private TextView registerTextView;
//
//    private FirebaseAuth mAuth;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_login);
//
//
//        if (isNetworkAvailable()) {
//
//            mAuth = FirebaseAuth.getInstance();
//
//            emailEditText = (EditText) findViewById(R.id.emailEditText);
//            passwordEditText = (EditText) findViewById(R.id.passwordEditText);
//            loginButton = (Button) findViewById(R.id.loginButton);
//            forgotPasswordTextView = (TextView) findViewById(R.id.forgot_password);
//            registerTextView = (TextView) findViewById(R.id.registerTextView);
//
////        fb
////        callbackManager = CallbackManager.Factory.create();
////        fbLoginButton = (LoginButton)findViewById(R.id.fbLoginButton);
//
//
//            Typeface openSansRegular = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
//            // Typeface toolbarFont = Typeface.createFromAsset(getAssets(), "fonts/Poppins-SemiBold.ttf");
//
//            emailEditText.setTypeface(openSansRegular);
//            passwordEditText.setTypeface(openSansRegular);
//            loginButton.setTypeface(openSansRegular);
//            forgotPasswordTextView.setTypeface(openSansRegular);
//            registerTextView.setTypeface(openSansRegular);
//
//
//            emailValidator = new EmailValidator();
//
//
//            loginButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    boolean valid = emailValidator.validate(emailEditText.getText().toString());
//
//
//                    if (valid) {
//
//
//                        signInWithEmailPassword(emailEditText.getText().toString(), passwordEditText.getText().toString());
//
//                    } else {
//
//                        Toast.makeText(LoginActivity.this, "Email Problem", Toast.LENGTH_SHORT).show();
//                    }
//
//
//                }
//            });
//
//
//            forgotPasswordTextView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent i = new Intent(LoginActivity.this, ForgotPassword.class);
//                    startActivity(i);
//                }
//            });
//
//
//            registerTextView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent i = new Intent(LoginActivity.this, SignupActivity.class);
//                    startActivity(i);
//                }
//            });
//
//        } else {
//
//
//            AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
//
//            alertDialog.setTitle("Info");
//            alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
//            alertDialog.setCanceledOnTouchOutside(false);
//            alertDialog.show();
//
//
//        }
//    }
//
//    private void updateUI() {
//        Intent i = new Intent(LoginActivity.this, MainActivity.class);
//        startActivity(i);
//    }
//
//    public void signInWithEmailPassword(String email, String password) {
//        FirebaseUser user = mAuth.getCurrentUser();
//
//
//        mAuth.signInWithEmailAndPassword(email, password)
//                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        if (task.isSuccessful()) {
//                            // Sign in success, update UI with the signed-in user's information
//
//                            FirebaseUser user = mAuth.getCurrentUser();
//
//                            if (user.isEmailVerified()) {
//
//                                Intent i = new Intent(LoginActivity.this, MainActivity.class);
//                                startActivity(i);
//                            }
//                            else {
//
//                                Toast.makeText(LoginActivity.this, "User is not verified !.",
//                                        Toast.LENGTH_SHORT).show();
//
//                            }
//
//                        } else {
//                            // If sign in fails, display a message to the user.
//
//                            Toast.makeText(LoginActivity.this, "Authentication failed.",
//                                    Toast.LENGTH_SHORT).show();
//
//                        }
//                    }
//                });
//    }
//
//
//    private boolean isNetworkAvailable() {
//        ConnectivityManager connectivityManager
//                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//    }
//
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//
//        FirebaseUser currentUser = mAuth.getCurrentUser();
//        if(currentUser!=null){
//            startActivity(new Intent(LoginActivity.this,MainActivity.class));
//        }
//
//    }
//}
