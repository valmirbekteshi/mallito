package besplay.com.mallito;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;


public class WebViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);


        TextView back = (TextView)findViewById(R.id.webViewBack);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });

       Bundle bundle = getIntent().getExtras();


        WebView webView = (WebView)findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());



        String link = bundle.getString("link");
        String documentType = bundle.getString("documentType");

        webView.loadUrl(link);




    }



}



