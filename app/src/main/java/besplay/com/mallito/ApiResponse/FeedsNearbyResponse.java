package besplay.com.mallito.ApiResponse;

/**
 * Created by Valmir on 5/12/2017.
 */


import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import besplay.com.mallito.Classes.FeedsNearby;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "feedsNearby"
})
public class FeedsNearbyResponse {

    @JsonProperty("feedsNearby")
    private List<FeedsNearby> feedsNearby = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("feedsNearby")
    public List<FeedsNearby> getFeedsNearby() {
        return feedsNearby;
    }

    @JsonProperty("feedsNearby")
    public void setFeedsNearby(List<FeedsNearby> feedsNearby) {
        this.feedsNearby = feedsNearby;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}