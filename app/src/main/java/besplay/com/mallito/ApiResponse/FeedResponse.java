package besplay.com.mallito.ApiResponse;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import besplay.com.mallito.Classes.Feed;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "feeds"
})
public class FeedResponse {

    @JsonProperty("feeds")
    private List<Feed> feeds = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("feeds")
    public List<Feed> getFeeds() {
        return feeds;
    }

    @JsonProperty("feeds")
    public void setFeeds(List<Feed> feeds) {
        this.feeds = feeds;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}