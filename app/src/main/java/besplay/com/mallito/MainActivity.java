package besplay.com.mallito;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.auth.FirebaseAuth;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import besplay.com.mallito.Adapter.DrawerAdapter;
import besplay.com.mallito.Adapter.FeedAdapter;
import besplay.com.mallito.ApiInterface.ApiInterface;
import besplay.com.mallito.ApiResponse.FeedResponse;
import besplay.com.mallito.ApiResponse.FeedsNearbyResponse;
import besplay.com.mallito.Classes.Cat;
import besplay.com.mallito.Classes.CategoriesResponse;
import besplay.com.mallito.Classes.Feed;
import besplay.com.mallito.Classes.FeedOnSaleResponse;
import besplay.com.mallito.Classes.FeedsNearby;
import besplay.com.mallito.Classes.FeedsOnSale;
import besplay.com.mallito.Classes.Subcategory;
import besplay.com.mallito.Classes.Subcategory_;
import besplay.com.mallito.Helpers.MallitoHelper;
import besplay.com.mallito.rest.ApiClient;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider.REQUEST_CHECK_SETTINGS;


public class MainActivity extends AppCompatActivity implements /*NavigationView.OnNavigationItemSelectedListener,*/ SearchView.OnQueryTextListener
        , OnLocationUpdatedListener {


    private static final String TAG = MainActivity.class.getSimpleName();
    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    List<Feed> productList;
    RecyclerView recyclerView;
    RecyclerView drawerList;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    RelativeLayout blackBackgroundLayout;
    private AlertDialog alertDialog;
    private FeedAdapter feedAdapter;
    LinearLayout llBottomSheet;
    BottomSheetBehavior bottomSheetBehavior;
    DrawerLayout drawer;
    private SliderLayout mDemoSlider;
    SwipeRefreshLayout swipeRefreshLayout;
    private int currentPage = 1;
    Button btnLocation;
    private Button btnOnSale;
    public static TextView navAll;
    public static TextView nav_second;
    public static TextView nav_third;
    Toolbar toolbar;
    List<Cat> menuList;
    RelativeLayout main;
    SearchView searchView;
    ImageView threeDot;
    MallitoHelper mallitoHelper;
    private static List<Cat> cachedMenuList = new ArrayList<>();
    private Cat secondTopCat;
    private Cat thirdTopCat;


    private String documentType = "";
    private String SearchQuery = "";
    boolean isVisible = false;
    boolean ispressed = false;
    private boolean fromMenu, onSale, nearby, fromSearch, fromPullToRefresh = false;
    //    private boolean onSale = false;
//    private boolean nearby = false;
//    boolean fromSearch = false;
    private boolean isScrollOnTop = true;
    private int categoryId = -1;
    private int documentNr = 0;


    double lat = 0, lng = 0;

    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (isNetworkAvailable()) {

            toolbar = (Toolbar) findViewById(R.id.my_toolbar);
            setSupportActionBar(toolbar);

            final int heightScreen = (int) (getResources().getDisplayMetrics().heightPixels);
            final int widthScreen = (int) (getResources().getDisplayMetrics().widthPixels);


            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            navAll = (TextView) findViewById(R.id.navAll);
            nav_second = (TextView) findViewById(R.id.nav_second);
            nav_third = (TextView) findViewById(R.id.nav_third);

            main = (RelativeLayout) findViewById(R.id.main);

            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,
                    drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);


            final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);


            menuList = new ArrayList<>();

            drawerList = (RecyclerView) findViewById(R.id.drawerList);
            recyclerView = (RecyclerView) findViewById(R.id.recycleView);
            recyclerView.setItemAnimator(new DefaultItemAnimator());


            //   keyboard hidden on start
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            btnOnSale = (Button) findViewById(R.id.btnOnSale);

            btnOnSale.setVisibility(View.GONE);

            btnOnSale.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Call<FeedOnSaleResponse> callOnSale = null;
                    currentPage = 1;
                    getProductsOnSale(callOnSale);
                }
            });


            staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(staggeredGridLayoutManager);
            recyclerView.setAdapter(feedAdapter);


            swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    currentPage = 1;
                    fromPullToRefresh = true;

                    Call<FeedResponse> call = null;
                    Call<FeedOnSaleResponse> callOnSale = null;
                    Call<FeedsNearbyResponse> nearbyFeeds = null;

                    if (onSale) {
                        if (categoryId > 0) {
                            callOnSale = apiService.fetchFeedsOnSale(documentType, categoryId, currentPage);
                        } else {
                            callOnSale = apiService.fetchFeedsOnSale(documentType, -1, currentPage);
                        }

                        getProductsOnSale(callOnSale);
                    } else if (nearby) {
                        if (categoryId > 0) {
                            nearbyFeeds = apiService.fetchFeedsNearby(documentType, categoryId, lat, lng, currentPage);
                        } else {
                            nearbyFeeds = apiService.fetchFeedsNearby(documentType, 1, lat, lng, currentPage);

                        }
                        GetFeedsForLocation(nearbyFeeds);
//                   GetNearbyFeeds(nearbyFeeds);

                    } else if (documentType.equals("")) {
                        currentPage = 1;
                        call = apiService.getFeeds(currentPage);
                        GetFeedsFromLoadMore(call);
                    } else if (fromSearch) {
                        //fromSearch
                        call = apiService.fetchFeedsForSearch(SearchQuery, currentPage);
                        performSearch(SearchQuery, currentPage);
                    } else if (fromMenu) {
                        call = apiService.fetchFeedsForCategoryAndEntity(documentType, categoryId, currentPage);
                        GetFeedsFromLoadMore(call);
                    } else {
                        call = apiService.getFeedsForEntity(documentType, currentPage);
                        GetFeedsFromLoadMore(call);
                    }


                }

            });


            Typeface toolbarFont = Typeface.createFromAsset(getAssets(), "fonts/Poppins-SemiBold.ttf");
            Typeface spinnerFont = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");


            searchView = (SearchView) findViewById(R.id.searchview);
            searchView.onActionViewExpanded();
            searchView.clearFocus();

            productList = new ArrayList<>();

            //main.requestFocus();
            searchView.setOnQueryTextListener(this);


            final TextView searchText = (TextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
            searchText.setTextSize(15);
            searchText.setTypeface(spinnerFont);


            TextView toolbarTitle = (TextView) findViewById(R.id.toolbarTitle);
            toolbarTitle.setTypeface(toolbarFont);
            toolbarTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Resources.Theme theme = getResources().newTheme();
                    theme.applyStyle(R.style.generalColor, false);
                    changeTheme(theme);
//                    productList.clear();

                    nav_second.setText("");
                    nav_third.setText("");
                    toolbar.setBackgroundResource(R.color.colorPrimary);
                    documentType = "";
                    currentPage = 1;

                    searchView.clearFocus();
                    searchText.setText("");
                    hamburgerVisibility(false);
                    btnOnSale.setVisibility(View.GONE);
                    btnLocation.setVisibility(View.VISIBLE);
                    fromMenu = false;

                    categoryId = -1;

                    Call<FeedResponse> call = null;
                    GetFeedsFromLoadMore(call);
                    staggeredGridLayoutManager.scrollToPosition(0);


                }
            });


            btnLocation = (Button) findViewById(R.id.btnLocation);


            btnLocation.setTypeface(spinnerFont);


            // this code won't execute IF permissions are not allowed, because in the line above there is return statement.
            btnLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // first check for permissions
                    if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION,
                                            android.Manifest.permission.INTERNET}
                                    , 10);
                        }
                        return;
                    }

                    boolean isEnabled = SmartLocation.with(getApplicationContext()).location().state().locationServicesEnabled();

                    if (isEnabled) {

                        getLocation();


                    } else {


//                        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
//                        alert.setMessage("Ju lutem aktivizoni Lokacionin !");
//                        alert.setNegativeButton("Cancel", null);
//                        alert.setPositiveButton("Aktivizo", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                                startActivityForResult(i, 100);
//                            }
//                        });
//
//                        alert.show();


                        displayLocationSettingsRequest(getApplicationContext());

                    }

                }


            });

            llBottomSheet = (LinearLayout) findViewById(R.id.bottom_sheet);
            bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);

            //bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);


            //get inches of device

//            DisplayMetrics dm = new DisplayMetrics();
//            getWindowManager().getDefaultDisplay().getMetrics(dm);
//            int width = dm.widthPixels;
//            int height = dm.heightPixels;
//            double wi = (double) width / (double) dm.xdpi;
//            double hi = (double) height / (double) dm.ydpi;
//            double x = Math.pow(wi, 2);
//            double y = Math.pow(hi, 2);
//            double screenInches = Math.sqrt(x + y);
//
//
//
//
//            if (screenInches <= 4) {
//                bottomSheetBehavior.setPeekHeight(40);
//                toolbarTitle.setTextSize(25);
//            }
//            else if (screenInches >= 4 && screenInches <= 5) {
//
//                bottomSheetBehavior.setPeekHeight(50);
//            }
//            else if (screenInches >= 5 && screenInches <= 6) {
//
//                bottomSheetBehavior.setPeekHeight(90);
//            }
//            else if (screenInches >= 7) {
//                toolbarTitle.setTextSize(35);
//                bottomSheetBehavior.setPeekHeight(43);
//                searchText.setTextSize(18);
//            }


            blackBackgroundLayout = (RelativeLayout) findViewById(R.id.blackBackground);

            blackBackgroundLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Resources.Theme theme = getResources().newTheme();

                    switch (documentType) {

                        case "":
                            theme.applyStyle(R.style.DefaultScene, false);
                            changeTheme(theme);
                            break;

                        case "gallery":
                            theme.applyStyle(R.style.galleryColor, false);
                            changeTheme(theme);
                            break;

                        case "product":
                            theme.applyStyle(R.style.productColor, false);
                            changeTheme(theme);

                            break;

                        case "jobpost":
                            theme.applyStyle(R.style.jobPostColor, false);
                            changeTheme(theme);

                            break;


                        case "blog":
                            theme.applyStyle(R.style.blogColor, false);
                            changeTheme(theme);

                            break;


                    }

                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            });


            bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {


                    if (newState == 4) {
                        blackBackgroundLayout.setVisibility(View.GONE);
                        isVisible = true;

                    } else if (newState == 3) {

                        blackBackgroundLayout.setVisibility(View.VISIBLE);
                        isVisible = false;
                    }


                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    blackBackgroundLayout.setVisibility(View.VISIBLE);


                }

            });


            initScroll();

            Call<FeedResponse> call = apiService.getFeeds(currentPage);
            call.enqueue(new Callback<FeedResponse>() {
                @Override
                public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
                    if (response.isSuccessful()) {

                        productList = response.body().getFeeds();
                        feedAdapter = new FeedAdapter(productList, getApplicationContext());
                        recyclerView.setAdapter(feedAdapter);
                    }
                }

                @Override
                public void onFailure(Call<FeedResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
                }
            });


            drawerList.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), drawerList,
                    new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            final Cat cat = menuList.get(position);
                            //showToast(cat.getId()+"");
                            //currentPage = 1;

                            Call<FeedResponse> call = apiService.fetchFeedsForCategoryAndEntity(documentType, cat.getId(), currentPage);
                            call.enqueue(new Callback<FeedResponse>() {
                                @Override
                                public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {

                                    if (response.isSuccessful()) {


                                        productList.clear();
                                        productList = response.body().getFeeds();
                                        recyclerView.setAdapter(feedAdapter);
                                        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                                        recyclerView.setLayoutManager(staggeredGridLayoutManager);
                                        feedAdapter = new FeedAdapter(productList, getApplicationContext());
                                        recyclerView.setAdapter(feedAdapter);

                                        switchCategories(cat);

                                        categoryId = cat.getId();

                                        fromMenu = true;
//                                        documentType = "product";
                                        initScroll();
                                    }
                                }

                                @Override
                                public void onFailure(Call<FeedResponse> call, Throwable t) {
                                    // Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }

                        @Override
                        public void onLongItemClick(View view, int position) {

                        }
                    }));


//==================
//            drawerList.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), drawerList,
//                    new RecyclerItemClickListener.OnItemClickListener() {
//                        @Override
//                        public void onItemClick(View view, int position) {
//                            final Cat cat = menuList.get(position);
//                            currentPage = 1;
//                            categoryId = cat.getId();
//
//                            Call<FeedResponse> call = apiService.fetchFeedsForCategoryAndEntity(documentType, cat.getId(), currentPage);
//                            call.enqueue(new Callback<FeedResponse>() {
//                                @Override
//                                public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
//
//                                    if (response.isSuccessful()) {
//
//
//                                        if (currentPage == 1) {
//                                            productList.clear();
//                                            productList = response.body().getFeeds();
//                                            fromMenu = true;
//
//                                            staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
//                                            recyclerView.setLayoutManager(staggeredGridLayoutManager);
//                                            feedAdapter = new FeedAdapter(productList, MainActivity.this);
//                                            recyclerView.setAdapter(feedAdapter);
//                                            initScroll();
//
//                                            switchCategories(cat);
//
//                                        }
//                                        else {
//
//                                            for (Feed feed : response.body().getFeeds()) {
//                                                productList.add(feed);
//                                            }
//                                            feedAdapter.notifyDataSetChanged();
//
//                                        }
//
//
//
////                                        documentType = "product";
//
//                                    }
//                                }
//
//                                @Override
//                                public void onFailure(Call<FeedResponse> call, Throwable t) {
//                                    Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
//                                }
//                            });
//                        }
//
//                        @Override
//                        public void onLongItemClick(View view, int position) {
//
//                        }
//                    }));

//=================


            recyclerView.addOnItemTouchListener(
                    new RecyclerItemClickListener(getApplicationContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            final Feed feeds = productList.get(position);


                            // alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                            alertDialog = new AlertDialog.Builder(MainActivity.this, R.style.DialogTheme).create();
                            LayoutInflater inflater = MainActivity.this.getLayoutInflater();
                            final View inflateView = inflater.inflate(R.layout.alert_product, null);


//                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                            lp.copyFrom(alertDialog.getWindow().getAttributes());
//                            lp.width = widthScreen;
//                            lp.height = heightScreen;
//                            alertDialog.getWindow().setAttributes(lp);

                            alertDialog.setCancelable(true);
                            alertDialog.setCanceledOnTouchOutside(true);
                            alertDialog.setView(inflateView);
                            alertDialog.show();


                            // if item is favorite set ispressed = true;

                            List<String> imgUrl = feeds.getImageUrls();


                            mDemoSlider = (SliderLayout) inflateView.findViewById(R.id.slider);
                            mDemoSlider.stopAutoCycle();


                            ImageView productImage = (ImageView) inflateView.findViewById(R.id.productImage);


                            if (imgUrl.size() > 1) {

                                productImage.setVisibility(View.GONE);

                                for (String element : imgUrl) {

                                    if (!element.equals("")) {
                                        TextSliderView textSliderView = new TextSliderView(getApplicationContext());
                                        textSliderView
                                                .image(element)
                                                .setScaleType(BaseSliderView.ScaleType.FitCenterCrop);

                                        mDemoSlider.addSlider(textSliderView);
                                    }
                                }
                            } else {

                                productImage.setVisibility(View.VISIBLE);


                                String thmb = feeds.getImageUrl().replaceAll("crp", "thumb");

                                //!TextUtils.isEmpty(feeds.getImageUrl()) ? feeds.getImageUrl() : R.drawable.ic_mallitoplaceholder


                                Glide.with(MainActivity.this).load(feeds.getImageUrl())
                                        .thumbnail(0.9f)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .placeholder(R.drawable.ic_mallitoplaceholder)
                                        .crossFade()
                                        .into(productImage);
                            }

                            mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Stack);
                            mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);


                            TextView productTitle = (TextView) inflateView.findViewById(R.id.productTitle);
                            TextView price = (TextView) inflateView.findViewById(R.id.price);
                            TextView newPrice = (TextView) inflateView.findViewById(R.id.newPrice);
                            TextView productLink = (TextView) inflateView.findViewById(R.id.productLink);
                            final TextView documentType = (TextView) inflateView.findViewById(R.id.documentType);
                            TextView sale = (TextView) inflateView.findViewById(R.id.sale);
                            Button productViewMore = (Button) inflateView.findViewById(R.id.productViewMore);
                            ImageView shareButton = (ImageView) inflateView.findViewById(R.id.shareButton);
                            //View br = (View) inflateView.findViewById(R.id.br);
                           // TextView createdAt = (TextView) inflateView.findViewById(R.id.createdAt);
                           // TextView photoCount = (TextView) inflateView.findViewById(R.id.photoCount);

                            ImageView close = (ImageView) inflateView.findViewById(R.id.close);
//                            if (imgUrl.size() > 1) {
//                                photoCount.setText("(" + imgUrl.size() + " photos)");
//                            } else {
//                                photoCount.setVisibility(View.GONE);
//                                br.setVisibility(View.GONE);
//                            }


                            mallitoHelper = new MallitoHelper();

                            int percentage = mallitoHelper.percentage(feeds);

                            if (percentage > 0) {
                                sale.setText(percentage + " % off");
                            } else {
                                sale.setVisibility(View.GONE);
                            }


                            close.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.cancel();
                                }
                            });


                            productTitle.setText(feeds.getTitle());

                            shareButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                    sharingIntent.setType("text/plain");
                                    String shareBody = feeds.getLink();
                                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                                }
                            });


//                        ispressed = false;
//                        final LottieAnimationView favv = (LottieAnimationView) inflateView.findViewById(R.id.favorite);
//                        favv.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//
//                                if (ispressed) {
//                                    favv.clearAnimation();
//
//                                    ChangeFavColor(Color.parseColor("#cccccc"), favv);
//                                    ispressed = false;
//
//                                } else {
//                                    ChangeFavColor(Color.RED, favv);
//                                    ispressed = true;
//                                }
//
//                            }
//                        });


//                            if(!feeds.getNewPrice().equals("") && feeds.getNewPrice()!=null && !feeds.getNewPrice().equals(String.valueOf(0))){
//
//                                newPrice.setText(feeds.getNewPrice()+"€");
//
//                            }
//                            else {
//
//                               newPrice.setVisibility(View.GONE);
//                            }
//
//                            if(!feeds.getPrice().equals("") && feeds.getPrice()!=null && !feeds.getPrice().equals(String.valueOf(0))){
//
//                              price.setText(feeds.getPrice()+"€");
//
//                            }
//                            else {
//
//                                price.setVisibility(View.GONE);
//                            }

//
//                            price.setText(feeds.getPrice()+" €");
//                            newPrice.setText(feeds.getNewPrice()+" €");
//

                            price.setVisibility(View.GONE);
                            newPrice.setVisibility(View.GONE);

                            if (feeds.getPrice() != null && !feeds.getPrice().equals("0") && !feeds.getPrice().equals(" ")) {

                                price.setVisibility(View.VISIBLE);
                                price.setTextColor((Color.parseColor("#999999")));
                                price.setText(feeds.getPrice() + " €");
                                price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            }
                            if (feeds.getNewPrice() != null && !feeds.getNewPrice().equals("0") && !feeds.getNewPrice().isEmpty()) {

                                newPrice.setText(feeds.getNewPrice() + " €");
                                newPrice.setVisibility(View.VISIBLE);

                            } else {

                                price.setPaintFlags(price.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                                price.setTextColor(Color.BLACK);
                            }


//
//                            price.setText(feeds.getPrice());
//                            newPrice.setText(feeds.getNewPrice());

                            Date date = null;
                            String createdAtPost = feeds.getCreatedAt();
                            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                date = fmt.parse(createdAtPost);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            //createdAt.setText(fmt.format(date));


                            productLink.setText(feeds.getPublisherName());
                           // documentType.setText(feeds.getDocumentType());
                            // price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                            switch (feeds.getDocumentType()) {

                                case "blog":
                                    //photoCount.setVisibility(View.VISIBLE);
                                    price.setVisibility(View.GONE);
                                    newPrice.setVisibility(View.GONE);
                                    productViewMore.setBackgroundResource(R.color.blogColor);
                                    //   inflateView.setBackgroundResource(R.color.servicesColor);
                                    break;

                                case "product":
                                   // photoCount.setVisibility(View.VISIBLE);
                                    productViewMore.setBackgroundResource(R.color.productColor);
                                    // inflateView.setBackgroundResource(R.color.productColor);
                                    break;

                                case "jobpost":
                                    btnLocation.setVisibility(View.GONE);
                                    price.setVisibility(View.GONE);
                                    newPrice.setVisibility(View.GONE);
                                    //photoCount.setVisibility(View.GONE);
                                    productViewMore.setBackgroundResource(R.color.jobPostColor);
                                    // inflateView.setBackgroundResource(R.color.jobPostColor);
                                    break;

                                case "gallery":
                                    price.setVisibility(View.GONE);
                                    newPrice.setVisibility(View.GONE);
                                   // photoCount.setVisibility(View.GONE);
                                    productViewMore.setBackgroundResource(R.color.galleryColor);
                                    // inflateView.setBackgroundResource(R.color.galleryColor);
                                    break;


                                case "":
                                    price.setVisibility(View.GONE);
                                    newPrice.setVisibility(View.GONE);
                                    //photoCount.setVisibility(View.GONE);
                                    productViewMore.setBackgroundResource(R.color.generalColor);
                                    // inflateView.setBackgroundResource(R.color.generalColor);
                                    break;
                            }

//

                            productViewMore.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    Intent i = new Intent(MainActivity.this, WebViewActivity.class);
                                    i.putExtra("link", feeds.getLink());
                                    startActivity(i);
                                }
                            });


                            // swipe to dismiss
//                            swipeDismissDialog = new SwipeDismissDialog.Builder(MainActivity.this)
//                                    .setView(dialog)
//                                    .build()
//                                    .show();


                        }

                        @Override
                        public void onLongItemClick(View view, int position) {


                        }
                    })
            );


            threeDot = (ImageView) findViewById(R.id.threeDot);
            threeDot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Resources.Theme theme = getResources().newTheme();
                    theme.applyStyle(R.style.DefaultScene, true);
                    changeTheme(theme);
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            });


            final ContextThemeWrapper wrapper = new ContextThemeWrapper(this, R.style.DefaultScene);
            changeTheme(wrapper.getTheme());


            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


        } else {


            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Info")
                    .setMessage("No Internet Connection,check your settings")
                    .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }

                    })
                    .show();


        }
    }


    private void changeTheme(final Resources.Theme theme) {
        final Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.dragsvg, theme);
        threeDot.setImageDrawable(drawable);
    }


    private void switchCategories(final Cat cat) {


        Typeface openSansSemiBold = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Semibold.ttf");

        navAll.setTypeface(openSansSemiBold);
        nav_second.setTypeface(openSansSemiBold);
        nav_third.setTypeface(openSansSemiBold);


        if (cat.getParentId() == 0 && cat.getSubcategories() != null) {
            nav_second.setText(" -> " + cat.getName() + " -> ");
            secondTopCat = cat;
        } else if (cat.getParentId() > 0 && cat.getSubcategories().size() == 0) {
            //showToast("no sub cats");
            nav_third.setText(cat.getName());
        } else {
            nav_third.setText(cat.getName());
            thirdTopCat = cat;
        }

        if (cat.getSubcategories() == null) {

        } else {
            menuList.clear();

            for (Subcategory sub : cat.getSubcategories()) {
                Cat category = new Cat();
                category.setId(sub.getId());
                category.setName(sub.getName());
                category.setDt(sub.getDt());
                category.setParentId(sub.getParentId());

                List<Subcategory> subcategory_list = new ArrayList<Subcategory>();

                if (sub.getSubcategories() == null) {

                } else {
                    for (Subcategory_ subcategory_ : sub.getSubcategories()) {
                        Subcategory subcategory = new Subcategory();
                        subcategory.setId(subcategory_.getId());
                        subcategory.setName(subcategory_.getName());
                        subcategory.setParentId(subcategory_.getParentId());
                        subcategory.setDt(subcategory_.getDt());
                        subcategory_list.add(subcategory);
                    }
                }

                category.setSubcategories(subcategory_list);
                menuList.add(category);
            }

            drawerList.setAdapter(new DrawerAdapter(menuList, MainActivity.this, drawerList));


        }

        navAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nav_second.setText("");
                nav_third.setText("");
                secondTopCat = null;
                thirdTopCat = null;
                getCategories();
            }
        });


        //second nav item onclick
        nav_second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuList.clear();
                nav_third.setText("");
                thirdTopCat = null;

                Call<CategoriesResponse> categoriesResponseCall = apiService.fetchMallitoCategories(documentNr);
                categoriesResponseCall.enqueue(new Callback<CategoriesResponse>() {
                    @Override
                    public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
                        if (response.isSuccessful()) {
                            cachedMenuList = response.body().getCats();

                            int id = secondTopCat.getId();

                            List<Subcategory> subcategories = new ArrayList<Subcategory>();
                            // List<Cat> cats = new ArrayList<Cat>();

                            for (Cat cat : cachedMenuList) {
                                for (Subcategory subcat : cat.getSubcategories()) {
                                    if (subcat.getParentId().equals(id)) {
                                        subcategories.add(subcat);

                                        Cat category = new Cat();
                                        category.setId(subcat.getId());
                                        category.setName(subcat.getName());
                                        category.setDt(subcat.getDt());
                                        category.setParentId(subcat.getParentId());

                                        List<Subcategory> subcategory_list = new ArrayList<Subcategory>();

                                        if (subcat.getSubcategories() == null) {

                                        } else {
                                            for (Subcategory_ subcategory_ : subcat.getSubcategories()) {
                                                Subcategory subcategory = new Subcategory();
                                                subcategory.setId(subcategory_.getId());
                                                subcategory.setName(subcategory_.getName());
                                                subcategory.setParentId(subcategory_.getParentId());
                                                subcategory.setDt(subcategory_.getDt());
                                                subcategory_list.add(subcategory);
                                            }
                                        }

                                        category.setSubcategories(subcategory_list);
                                        menuList.add(category);
                                    }
                                }
                            }
                        }
                        drawerList.setAdapter(new DrawerAdapter(menuList, MainActivity.this, drawerList));

                    }


                    @Override
                    public void onFailure(Call<CategoriesResponse> call, Throwable t) {
                        //Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


        //navThirdTop on click


        nav_third.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuList.clear();
//                nav_third.setText("");
//                thirdTopCat = null;

                Call<CategoriesResponse> categoriesResponseCall = apiService.fetchMallitoCategories(documentNr);
                categoriesResponseCall.enqueue(new Callback<CategoriesResponse>() {
                    @Override
                    public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
                        if (response.isSuccessful()) {
                            cachedMenuList = response.body().getCats();

                            int id = thirdTopCat.getId();

                            List<Subcategory> subcategories = new ArrayList<Subcategory>();
                            // List<Cat> cats = new ArrayList<Cat>();

                            for (Cat cat : cachedMenuList) {
                                for (Subcategory subcat : cat.getSubcategories()) {
                                    if (subcat.getParentId().equals(id)) {
                                        subcategories.add(subcat);

                                        Cat category = new Cat();
                                        category.setId(subcat.getId());
                                        category.setName(subcat.getName());
                                        category.setDt(subcat.getDt());
                                        category.setParentId(subcat.getParentId());

                                        List<Subcategory> subcategory_list = new ArrayList<Subcategory>();

                                        if (subcat.getSubcategories() == null) {

                                        } else {
                                            for (Subcategory_ subcategory_ : subcat.getSubcategories()) {
                                                Subcategory subcategory = new Subcategory();
                                                subcategory.setId(subcategory_.getId());
                                                subcategory.setName(subcategory_.getName());
                                                subcategory.setParentId(subcategory_.getParentId());
                                                subcategory.setDt(subcategory_.getDt());
                                                subcategory_list.add(subcategory);
                                            }
                                        }

                                        category.setSubcategories(subcategory_list);
                                        menuList.add(category);
                                    }
                                }
                            }
                        }
                        drawerList.setAdapter(new DrawerAdapter(menuList, MainActivity.this, drawerList));

                    }


                    @Override
                    public void onFailure(Call<CategoriesResponse> call, Throwable t) {
//                        Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }


//    private void ChangeFavColor(@ColorInt int color, LottieAnimationView favv) {
//        // Clear all color filters
//        favv.clearColorFilters();
//
//        final PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(color, PorterDuff.Mode.DARKEN);
//
//        // Adding a color filter to the whole view
//        favv.addColorFilter(colorFilter);
//        favv.setImageAlpha(0);
//        favv.playAnimation();
//    }

    private void getLocation() {
        SmartLocation.with(getApplicationContext()).location()
                .start(new OnLocationUpdatedListener() {

                    @Override
                    public void onLocationUpdated(Location location) {
                        lat = location.getLatitude();
                        lng = location.getLongitude();

                        currentPage = 1;
                        Call<FeedsNearbyResponse> nearbyFeeds = null;
                        GetFeedsForLocation(nearbyFeeds);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

//        IntentResult resultQr = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
//        if(resultQr!=null){
//
//            if(resultQr.getContents()==null){
//                showToast("You cancelled the scanning !");
//            }
//            else{
//                showToast(""+resultQr.getContents());
//                Intent intent = new Intent(MainActivity.this,WebViewActivity.class);
//                intent.putExtra("link",resultQr.getContents());
//                startActivity(intent);
//            }
//        }
//        else{

        super.onActivityResult(requestCode, resultCode, data);
//        }

//valmir
        switch (requestCode) {
            case 100:
                getLocation();
                //finishActivity(100);
                break;

            case REQUEST_CHECK_SETTINGS:
                getLocation();
                break;
        }
    }

    public void GetFeedsForLocation(Call<FeedsNearbyResponse> nearbyFeeds) {
        if (documentType.equals("")) {
            documentType = "all";
            categoryId = -1;
        }

        nearbyFeeds = apiService.fetchFeedsNearby(documentType, categoryId, lat, lng, currentPage);
        //Call<FeedsNearbyResponse> feedNearbyCall = apiService.fetchFeedsNearby(documentType, categoryId, lat, lng, currentPage);
        nearbyFeeds.enqueue(new Callback<FeedsNearbyResponse>() {
            @Override
            public void onResponse(Call<FeedsNearbyResponse> call, Response<FeedsNearbyResponse> response) {
                if (response.isSuccessful()) {

                    if (fromPullToRefresh) {
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    if (currentPage == 1) {
                        productList.clear();
                    }

                    for (FeedsNearby feedNearby : response.body().getFeedsNearby()) {
                        Feed feed = new Feed();
                        feed.setId(feedNearby.getId());
                        feed.setImageUrl(feedNearby.getImageUrl());
                        feed.setImageUrls(feedNearby.getImageUrls());
                        feed.setSvg(feedNearby.getSvg());
                        feed.setSvgColor(feedNearby.getSvgColor());
                        feed.setLink(feedNearby.getLink());
                        feed.setPrice(feedNearby.getPrice());
                        feed.setNewPrice(feedNearby.getNewPrice());
                        feed.setTitle(feedNearby.getTitle());
                        feed.setDescription(feedNearby.getDescription());
                        feed.setKeywords(feedNearby.getKeywords());
                        feed.setRootCategory(feedNearby.getRootCategory());
                        feed.setSubCategory(feedNearby.getSubCategory());
                        feed.setSubSubCategory(feedNearby.getSubSubCategory());
                        feed.setCreatedAt(feedNearby.getCreatedAt());
                        feed.setViews(feedNearby.getViews());
                        feed.setTenantId(feedNearby.getTenantId());
                        feed.setPublisherName(feedNearby.getPublisherName());
                        feed.setDocumentType(feedNearby.getDocumentType());
                        feed.setLocation(feedNearby.getLocation());

                        productList.add(feed);
                    }

                    if (currentPage == 1) {
                        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                        recyclerView.setLayoutManager(staggeredGridLayoutManager);
                        feedAdapter = new FeedAdapter(productList, getApplicationContext());
                        recyclerView.setAdapter(feedAdapter);

                        nearby = true;
                        initScroll();
                    } else {
                        feedAdapter.notifyDataSetChanged();
                    }

                }
            }

            @Override
            public void onFailure(Call<FeedsNearbyResponse> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void initScroll() {
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(staggeredGridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {

                fromPullToRefresh = false;
                currentPage = page + 1;
                int curSize = feedAdapter.getItemCount();
                loadMoreItems();

                //showToast(""+currentPage);
                feedAdapter.notifyItemRangeInserted(curSize, productList.size() - 1);

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.toolbar.getMenu().findItem(R.id.menuRight).setVisible(false);
//        this.toolbar.getMenu().findItem(R.id.settings).setVisible(true);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        int id = item.getItemId();


//        if (id == R.id.account) {
//
//            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
//            startActivity(i);
//
//        }


//        if (id == R.id.settings) {
//            Intent i = new Intent(MainActivity.this,QRReader.class);
//            startActivity(i);

//            IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
//            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
//            integrator.setPrompt("Scan");
//            integrator.setCameraId(0);
//            integrator.setBeepEnabled(false);
//            integrator.setBarcodeImageEnabled(false);
//            integrator.initiateScan();


//            FirebaseAuth fAuth = FirebaseAuth.getInstance();
//            if (fAuth == null) {
//                showToast("The user is already signout");
//            } else {
//                fAuth.signOut();
//                Intent i = new Intent(MainActivity.this, LoginActivity.class);
//                startActivity(i);
//            }

//        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.menuRight) {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);
            } else {
                drawer.openDrawer(GravityCompat.END);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    void hamburgerVisibility(boolean vlera) {

        this.toolbar.getMenu().findItem(R.id.menuRight).setVisible(vlera);
    }

    private void clearCategories() {
        nav_second.setText("");
        nav_third.setText("");
    }

    public void BottomSheetOnClick(View view) {
        // onSale = false;
        //fromMenu = false;
        nearby = false;
        fromSearch = false;


        final Resources.Theme theme = getResources().newTheme();

        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);


        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);


        documentType = view.getTag().toString();


        Call<FeedResponse> call = apiService.getFeedsForEntity(documentType, currentPage);


        switch (documentType) {

            case "":
                theme.applyStyle(R.style.DefaultScene, false);
                changeTheme(theme);
                hamburgerVisibility(false);
                btnOnSale.setVisibility(View.GONE);
//                call = apiService.getFeeds(currentPage);
                toolbar.setBackgroundResource(R.color.colorPrimary);
                btnLocation.setVisibility(View.VISIBLE);
                break;


            case "product":
                documentNr = 1;
                theme.applyStyle(R.style.productColor, false);
                changeTheme(theme);
                btnOnSale.setVisibility(View.VISIBLE);
                hamburgerVisibility(true);
                toolbar.setBackgroundResource(R.color.productColor);
                break;

            case "jobpost":

                clearCategories();
                theme.applyStyle(R.style.jobPostColor, false);
                changeTheme(theme);
                hamburgerVisibility(false);
                btnLocation.setVisibility(View.GONE);
                btnOnSale.setVisibility(View.GONE);
                toolbar.setBackgroundResource(R.color.jobPostColor);
                break;


            case "blog":
                documentNr = 3;
                clearCategories();
                theme.applyStyle(R.style.blogColor, false);
                changeTheme(theme);
                btnLocation.setVisibility(View.VISIBLE);
                hamburgerVisibility(true);
                btnOnSale.setVisibility(View.INVISIBLE);
                toolbar.setBackgroundResource(R.color.blogColor);
                break;


            case "gallery":
                documentNr = 4;
                clearCategories();
                theme.applyStyle(R.style.galleryColor, false);
                changeTheme(theme);
                hamburgerVisibility(true);
                btnOnSale.setVisibility(View.GONE);
                toolbar.setBackgroundResource(R.color.galleryColor);
                break;


        }

//        Call<CategoriesResponse> categoriesResponseCall = apiService.fetchMallitoCategories(documentNr);
//        categoriesResponseCall.enqueue(new Callback<CategoriesResponse>() {
//            @Override
//            public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
//                if (response.isSuccessful()) {
//                    drawerList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
//                    menuList = response.body().getCats();
//                    drawerList.setAdapter(new DrawerAdapter(menuList, MainActivity.this, drawerList));
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CategoriesResponse> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
//            }
//        });


        getCategories();


        call.enqueue(new Callback<FeedResponse>() {
            @Override
            public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
                if (response.isSuccessful()) {

                    staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                    recyclerView.setLayoutManager(staggeredGridLayoutManager);

                    productList = response.body().getFeeds();
                    feedAdapter = new FeedAdapter(productList, getApplicationContext());
                    recyclerView.setAdapter(feedAdapter);

                    initScroll();
                }
            }

            @Override
            public void onFailure(Call<FeedResponse> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getCategories() {
        Call<CategoriesResponse> categoriesResponseCall = apiService.fetchMallitoCategories(documentNr);
        categoriesResponseCall.enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
                if (response.isSuccessful()) {
                    cachedMenuList = response.body().getCats();
                    drawerList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                    menuList = response.body().getCats();
                    drawerList.setAdapter(new DrawerAdapter(menuList, MainActivity.this, drawerList));
                }
            }

            @Override
            public void onFailure(Call<CategoriesResponse> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (isScrollOnTop) {
            super.onBackPressed();
        } else {
            recyclerView.smoothScrollToPosition(0);
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);
            } else {
                // go to position 0
                //  super.onBackPressed();
            }
            isScrollOnTop = true;
        }
    }

    public void performSearch(String query, final int currentPage) {
        fromSearch = true;
        SearchQuery = query;


        if (TextUtils.isEmpty(query)) {
            showToast("Teksti eshte i zbrazet !");
        }


        Call<FeedResponse> call = apiService.fetchFeedsForSearch(SearchQuery, currentPage);

        call.enqueue(new Callback<FeedResponse>() {
            @Override
            public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
                if (response.isSuccessful()) {

                    if (currentPage == 1) {
                        productList = response.body().getFeeds();

                        if (productList.isEmpty()) {
                            showToast("Nuk eshte gjetur asnje rezultat me kete fjale  " + SearchQuery);
                        }

                        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                        recyclerView.setLayoutManager(staggeredGridLayoutManager);
                        feedAdapter = new FeedAdapter(productList, MainActivity.this);
                        recyclerView.setAdapter(feedAdapter);
                        initScroll();
                    } else {
                        for (Feed feed : response.body().getFeeds()) {
                            productList.add(feed);
                        }
                        feedAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<FeedResponse> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });


    }


    @Override
    public boolean onQueryTextSubmit(String query) {

        btnOnSale.setVisibility(View.GONE);
        currentPage = 1;
        performSearch(query, currentPage);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

//        //musa
//        if (TextUtils.isEmpty(newText)) {
//            showToast("Teksti eshte i zbrazet !");
//            productList.clear();
//            recyclerView.smoothScrollToPosition(1);
//            documentType = "";
//            Call<FeedResponse> call = null;
//            GetFeedsFromLoadMore(call);
//
//
//            recyclerView.requestFocus();
//
//        }
        return false;
    }


    private void getProductsOnSale(Call<FeedOnSaleResponse> callOnSale) {
        onSale = true;
        callOnSale = apiService.fetchFeedsOnSale("product", categoryId, currentPage);
        callOnSale.enqueue(new Callback<FeedOnSaleResponse>() {
            @Override
            public void onResponse(Call<FeedOnSaleResponse> call, Response<FeedOnSaleResponse> response) {
                if (response.isSuccessful()) {

                    if (fromPullToRefresh) {
                        swipeRefreshLayout.setRefreshing(false);
                    }


                    if (currentPage == 1) {
                        productList.clear();
                    }


                    for (FeedsOnSale feedonsale : response.body().getFeedsOnSale()) {
                        Feed feed = new Feed();
                        feed.setId(feedonsale.getId());
                        feed.setImageUrl(feedonsale.getImageUrl());
                        feed.setImageUrls(feedonsale.getImageUrls());
                        feed.setSvg(feedonsale.getSvg());
                        feed.setSvgColor(feedonsale.getSvgColor());
                        feed.setLink(feedonsale.getLink());
                        feed.setPrice(feedonsale.getPrice());
                        feed.setNewPrice(feedonsale.getNewPrice());
                        feed.setTitle(feedonsale.getTitle());
                        feed.setDescription(feedonsale.getDescription());
                        feed.setKeywords(feedonsale.getKeywords());
                        feed.setRootCategory(feedonsale.getRootCategory());
                        feed.setSubCategory(feedonsale.getSubCategory());
                        feed.setSubSubCategory(feedonsale.getSubSubCategory());
                        feed.setCreatedAt(feedonsale.getCreatedAt());
                        feed.setViews(feedonsale.getViews());
                        feed.setTenantId(feedonsale.getTenantId());
                        feed.setPublisherName(feedonsale.getPublisherName());
                        feed.setDocumentType(feedonsale.getDocumentType());
                        feed.setLocation(feedonsale.getLocation());

                        productList.add(feed);

                    }

                    if (currentPage == 1) {
                        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                        recyclerView.setLayoutManager(staggeredGridLayoutManager);
                        feedAdapter = new FeedAdapter(productList, getApplicationContext());
                        recyclerView.setAdapter(feedAdapter);
                        onSale = true;
                        initScroll();
                    } else {

                        feedAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<FeedOnSaleResponse> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void loadMoreItems() {
        //valmir
        Call<FeedResponse> call = null;
        Call<FeedOnSaleResponse> callOnSale = null;
        Call<FeedsNearbyResponse> nearbyFeeds = null;


        if (onSale) {
            if (categoryId > 0) {
                callOnSale = apiService.fetchFeedsOnSale(documentType, categoryId, currentPage);
            } else {
                callOnSale = apiService.fetchFeedsOnSale(documentType, -1, currentPage);
            }
//            getProductsOnSale(callOnSale);
            getProductsOnSale(callOnSale);

        } else if (nearby) {
            if (categoryId > 0) {
                nearbyFeeds = apiService.fetchFeedsNearby(documentType, categoryId, lat, lng, currentPage);
            } else {
                nearbyFeeds = apiService.fetchFeedsNearby(documentType, -1, lat, lng, currentPage);

            }
            GetFeedsForLocation(nearbyFeeds);
//            GetNearbyFeeds(nearbyFeeds);

        } else if (fromSearch) {
            performSearch(SearchQuery, currentPage);
        } else if (fromMenu) {
            call = apiService.fetchFeedsForCategoryAndEntity(documentType, categoryId, currentPage);
            GetFeedsFromLoadMore(call);
        } else if (documentType.equals("")) {
            call = apiService.getFeeds(currentPage);
            GetFeedsFromLoadMore(call);
        } else {
            call = apiService.getFeedsForEntity(documentType, currentPage);
            GetFeedsFromLoadMore(call);
        }


//        final LottieAnimationView paginationView = (LottieAnimationView) findViewById(R.id.paginationView);
//
//        paginationView.setVisibility(View.VISIBLE);
//        paginationView.setAnimation("pagination.json", LottieAnimationView.CacheStrategy.Strong);
//        paginationView.playAnimation();
//        paginationView.setProgress(0.5f);

        isScrollOnTop = false;
    }

    private void GetFeedsFromLoadMore(Call<FeedResponse> call) {
//musa

        if (documentType.equals("")) {
            call = apiService.getFeeds(currentPage);
        } else if (fromMenu) {

            call = apiService.fetchFeedsForCategoryAndEntity(documentType, categoryId, currentPage);

        } else {
            call = apiService.getFeedsForEntity(documentType, currentPage);

        }


        call.enqueue(new Callback<FeedResponse>() {
            @Override
            public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
                if (response.isSuccessful()) {

                    if (currentPage == 1) {
                        productList.clear();

                    }

                    if (fromPullToRefresh) {
                        swipeRefreshLayout.setRefreshing(false);
                    }


                    List<Feed> feedList = response.body().getFeeds();

                    for (Feed feed : feedList) {
                        productList.add(feed);
                    }

                    feedAdapter.notifyDataSetChanged();
                    // paginationView.setVisibility(View.GONE);


                }
            }

            @Override
            public void onFailure(Call<FeedResponse> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onLocationUpdated(Location location) {
        lat = location.getLatitude();
        lng = location.getLongitude();

    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onStop() {
        super.onStop();
        SmartLocation.with(getApplicationContext()).location().stop();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                SmartLocation.with(getApplicationContext()).location()
                        .start(new OnLocationUpdatedListener() {

                            @Override
                            public void onLocationUpdated(Location location) {


                                lat = location.getLatitude();
                                lng = location.getLongitude();

                                currentPage = 1;

                                Call<FeedsNearbyResponse> nearbyFeeds = null;
//                                "product", -1, location.getLatitude(), location.getLongitude(), currentPage
                                GetFeedsForLocation(nearbyFeeds);

                            }
                        });
                break;
            default:
                break;
        }
    }


    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        final LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(TAG, "All location settings are satisfied.");

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    public void showToast(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();

    }


}







