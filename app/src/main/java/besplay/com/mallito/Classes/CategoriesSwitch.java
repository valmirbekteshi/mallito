package besplay.com.mallito.Classes;

/**
 * Created by Valmir on 8/25/2017.
 */

public class CategoriesSwitch {
    private int id;
    private String name;
    private int parentId;
    private int dt;

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getDt() {
        return dt;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

//    public CategoriesSwitch(int id, String name,int parentId,int dt) {
//        this.id = id;
//        this.name = name;
//        this.parentId = parentId;
//        this.dt = dt;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
