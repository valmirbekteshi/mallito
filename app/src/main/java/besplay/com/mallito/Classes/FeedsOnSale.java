package besplay.com.mallito.Classes;

/**
 * Created by Visari on 5/3/2017.
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "imageUrl",
        "imageUrls",
        "svg",
        "svg_color",
        "link",
        "price",
        "new_price",
        "title",
        "description",
        "keywords",
        "rootCategory",
        "sub_category",
        "sub_sub_category",
        "created_at",
        "views",
        "tenantId",
        "publisherName",
        "documentType",
        "location"
})
public class FeedsOnSale {

    @JsonProperty("id")
    private String id;
    @JsonProperty("imageUrl")
    private String imageUrl;
    @JsonProperty("imageUrls")
    private List<String> imageUrls = null;
    @JsonProperty("svg")
    private Object svg;
    @JsonProperty("svg_color")
    private Object svgColor;
    @JsonProperty("link")
    private String link;
    @JsonProperty("price")
    private String price;
    @JsonProperty("new_price")
    private String newPrice;
    @JsonProperty("title")
    private String title;
    @JsonProperty("description")
    private Object description;
    @JsonProperty("keywords")
    private Object keywords;
    @JsonProperty("rootCategory")
    private Integer rootCategory;
    @JsonProperty("sub_category")
    private Integer subCategory;
    @JsonProperty("sub_sub_category")
    private Integer subSubCategory;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("views")
    private Integer views;
    @JsonProperty("tenantId")
    private Integer tenantId;
    @JsonProperty("publisherName")
    private String publisherName;
    @JsonProperty("documentType")
    private String documentType;
    @JsonProperty("location")
    private Location location;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("imageUrl")
    public String getImageUrl() {
        return imageUrl;
    }

    @JsonProperty("imageUrl")
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @JsonProperty("imageUrls")
    public List<String> getImageUrls() {
        return imageUrls;
    }

    @JsonProperty("imageUrls")
    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    @JsonProperty("svg")
    public Object getSvg() {
        return svg;
    }

    @JsonProperty("svg")
    public void setSvg(Object svg) {
        this.svg = svg;
    }

    @JsonProperty("svg_color")
    public Object getSvgColor() {
        return svgColor;
    }

    @JsonProperty("svg_color")
    public void setSvgColor(Object svgColor) {
        this.svgColor = svgColor;
    }

    @JsonProperty("link")
    public String getLink() {
        return link;
    }

    @JsonProperty("link")
    public void setLink(String link) {
        this.link = link;
    }

    @JsonProperty("price")
    public String getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(String price) {
        this.price = price;
    }

    @JsonProperty("new_price")
    public String getNewPrice() {
        return newPrice;
    }

    @JsonProperty("new_price")
    public void setNewPrice(String newPrice) {
        this.newPrice = newPrice;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("description")
    public Object getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(Object description) {
        this.description = description;
    }

    @JsonProperty("keywords")
    public Object getKeywords() {
        return keywords;
    }

    @JsonProperty("keywords")
    public void setKeywords(Object keywords) {
        this.keywords = keywords;
    }

    @JsonProperty("rootCategory")
    public Integer getRootCategory() {
        return rootCategory;
    }

    @JsonProperty("rootCategory")
    public void setRootCategory(Integer rootCategory) {
        this.rootCategory = rootCategory;
    }

    @JsonProperty("sub_category")
    public Integer getSubCategory() {
        return subCategory;
    }

    @JsonProperty("sub_category")
    public void setSubCategory(Integer subCategory) {
        this.subCategory = subCategory;
    }

    @JsonProperty("sub_sub_category")
    public Integer getSubSubCategory() {
        return subSubCategory;
    }

    @JsonProperty("sub_sub_category")
    public void setSubSubCategory(Integer subSubCategory) {
        this.subSubCategory = subSubCategory;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("views")
    public Integer getViews() {
        return views;
    }

    @JsonProperty("views")
    public void setViews(Integer views) {
        this.views = views;
    }

    @JsonProperty("tenantId")
    public Integer getTenantId() {
        return tenantId;
    }

    @JsonProperty("tenantId")
    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    @JsonProperty("publisherName")
    public String getPublisherName() {
        return publisherName;
    }

    @JsonProperty("publisherName")
    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    @JsonProperty("documentType")
    public String getDocumentType() {
        return documentType;
    }

    @JsonProperty("documentType")
    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    @JsonProperty("location")
    public Location getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(Location location) {
        this.location = location;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}