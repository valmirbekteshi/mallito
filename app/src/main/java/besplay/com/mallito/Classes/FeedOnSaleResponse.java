package besplay.com.mallito.Classes;

/**
 * Created by Visari on 5/3/2017.
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "feedsOnSale"
})
public class FeedOnSaleResponse {

    @JsonProperty("feedsOnSale")
    private List<FeedsOnSale> feedsOnSale = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("feedsOnSale")
    public List<FeedsOnSale> getFeedsOnSale() {
        return feedsOnSale;
    }

    @JsonProperty("feedsOnSale")
    public void setFeedsOnSale(List<FeedsOnSale> feedsOnSale) {
        this.feedsOnSale = feedsOnSale;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}