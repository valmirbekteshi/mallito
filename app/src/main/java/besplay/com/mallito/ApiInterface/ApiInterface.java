package besplay.com.mallito.ApiInterface;

import besplay.com.mallito.ApiResponse.FeedResponse;
import besplay.com.mallito.ApiResponse.FeedsNearbyResponse;
import besplay.com.mallito.Classes.CategoriesResponse;
import besplay.com.mallito.Classes.FeedOnSaleResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface ApiInterface {


    @GET("fetchFeeds/{page}")
    Call<FeedResponse> getFeeds(@Path("page") int page);


    @GET("fetchFeedsForEntity/{documentType}/{page}")
    Call<FeedResponse> getFeedsForEntity(@Path("documentType") String documentType, @Path("page") int page);

    @GET("fetchFeedsForSearch/{query}/{page}")
    Call<FeedResponse> fetchFeedsForSearch(@Path("query") String query, @Path("page") int page);

    @GET("fetchFeedsForCategoryAndEntity/{documentType}/{category}/{page}")
    Call<FeedResponse> fetchFeedsForCategoryAndEntity(@Path("documentType") String documentType, @Path("category") int category, @Path("page") int page);

    @GET("fetchMallitoCategories/{documentNr}")
    Call<CategoriesResponse> fetchMallitoCategories(@Path("documentNr") int documentNr);

    @GET("fetchFeedsOnSale/{document}/{categoryId}/{page}")
    Call<FeedOnSaleResponse> fetchFeedsOnSale(@Path("document") String document, @Path("categoryId")
            int categoryId, @Path("page") int page);

    @GET("feetchFeedsNearby/{document}/{categoryId}/{lat}/{lon}/{page}")
    Call<FeedsNearbyResponse> fetchFeedsNearby(@Path("document") String document, @Path("categoryId") int categoryId, @Path("lat") double lat,
                                               @Path("lon") double lon, @Path("page") int page);






}
