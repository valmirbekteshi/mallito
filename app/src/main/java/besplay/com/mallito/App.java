package besplay.com.mallito;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatDelegate;

/**
 * Created by Valmir on 9/4/2017.
 */



    class SplashScreenHelper implements Application.ActivityLifecycleCallbacks {

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            // apply the actual theme
            activity.setTheme(R.style.AppTheme);

            try {
                ActivityInfo activityInfo = activity.getPackageManager()
                        .getActivityInfo(activity.getComponentName(), PackageManager.GET_META_DATA);

                Bundle metaData = activityInfo.metaData;

                int theme;
                if (metaData != null) {
                    theme = metaData.getInt("theme", R.style.AppTheme);
                } else {
                    theme = R.style.AppTheme;
                }

                activity.setTheme(theme);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }


        }



    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    // ... other callbacks are empty
    }

    public class App extends Application {

        static {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }


        @Override
        protected void attachBaseContext(Context context) {
            super.attachBaseContext(context);
            MultiDex.install(this);
        }

        @Override
        public void onCreate() {
            super.onCreate();



            // register the util to remove splash screen after loading
            registerActivityLifecycleCallbacks(new SplashScreenHelper());




        }


    }

