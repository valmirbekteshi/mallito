package besplay.com.mallito.rest;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by Valmir on 3/27/2017.
 */

public class ApiClient {

    public static  final String BASE_URL = "https://mallito.com/api/";

    private  static Retrofit retrofit = null;

    //

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(JacksonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}